import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-general',
  templateUrl: './general.component.html',
  styleUrls: ['./general.component.scss']
})
export class GeneralComponent implements OnInit {
  test = 'it test string';
  title = 'test title';
  date = new Date();

  users: Array<any> = [
    {firstName: 'Ivan', lastName: 'Ivanov'},
    {firstName: 'Petr', lastName: 'Petrov'},
    {firstName: 'Sergey', lastName: 'Gvozdetskiy'},
    {firstName: 'Alina', lastName: 'Gvozdetskaya'}
  ];

  onButtonClicked() {
    this.test = 'button alive';
  }
  constructor() { }

  ngOnInit() {
  }

}
